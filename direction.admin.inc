<?php

/**
 * @file
 * Settings form.
 */

/**
 * Module settings page.
 */
function direction_admin_settings($form, &$form_state) {
  $direction_settings= variable_get("direction_settings", array());

  $options       = direction_get_possible_fields('user');
  $form['direction_user_geofield'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select geofield from user entity'),
    '#description'   => t(""),
    '#default_value' => variable_get('direction_user_geofield', ''),
    '#options'       => $options,
  );  

  $options       = direction_get_possible_fields('node');
  $form['direction_node_geofield'] = array(
    '#type'          => 'checkboxes',
    '#title'         => t('Select geofield from node entity'),
    '#description'   => t(""),
    '#default_value' => variable_get('direction_node_geofield', ''),
    '#options'       => $options,
  );  
  
  $form['direction_provider'] = array(
    '#type'          => 'select',
    '#title'         => t('Select direction provicer'),
    '#description'   => t("Google: (2500 requests per day / 5 requests per second). OSRM: (1000 request per day / 5 requests per second). Please see the actual tos!"),
    '#default_value' => variable_get('direction_provider', 'osrm'),
    '#options'       => array(
      'osrm' => 'Open Street Route Map',
      'google'  => 'Google Directions',
    ),
  );  
  
  $form['direction_user_agent'] = array(
    '#type'        => 'textfield',
    '#title'       => t('User Agent'),
    '#description' => t('Type in your custom user agent. It could be your contact email address, to avoid ip blocking. So your provider can you.'),
    '#default_value' => variable_get('direction_user_agent', 'drupal'),
    '#required'      => FALSE,
  );
  
  $form['direction_google_api_key'] = array(
    '#type'        => 'textfield',
    '#title'       => t('Google Maps API Key'),
    '#description' => t('Obtain a free Google Geocoding API Key at <a href="@link">@link</a>', array('@link' => 'https://developers.google.com/maps/documentation/geocoding/#api_key')),
    '#default_value' => variable_get('direction_google_api_key', ''),
    '#required'      => FALSE,
  );

  $form['direction_request_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay between requests (in milliseconds)'),
    '#description' => t('Adds a delay between requests, to avoid query limit errors. 200ms is recommended.'),
    '#default_value' => variable_get('direction_request_delay', '200'),
    '#size' => 10,
  );


  $form['#submit'][] = 'direction_admin_settings_submit';
  return system_settings_form($form);
}



function direction_admin_settings_submit($form, &$form_state) {
  $direction_settings = variable_get("direction_settings", array());
  $direction_settings['direction_google_api_key'] = trim($form_state['values']['direction_google_api_key']);
  variable_set("direction_settings", $direction_settings);
}
