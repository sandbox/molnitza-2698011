<?php

function direction_views_views_data() {
  $data[DB_TABLE_NAME]['table']['group'] = t('Route');

  $data[DB_TABLE_NAME]['table']['base'] = array(
    'field' => 'nid', // This is the identifier field for the view.
    'title' => t('Route'),
    'help' => t('Node related to route.'),
    'weight' => -10,
  );

  $data[DB_TABLE_NAME]['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );


  $data[DB_TABLE_NAME]['nid'] = array(
    'title' => t('Nid'),
    'help' => t('NID'),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'nid',
    ),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
      'title' => t('Node'),
      'help' => t('Node realated to route.'),
    ),
  );

  $data[DB_TABLE_NAME]['uid'] = array(
    'title' => t('Uid'),
    'help' => t('User related to route.'),
    'argument' => array(
      'handler' => 'views_handler_argument',
      'name field' => 'uid',
    ),
    'relationship' => array(
      'base' => 'users',
      'base field' => 'uid',
      'handler' => 'views_handler_relationship',
      'label' => t('User'),
      'title' => t('User'),
      'help' => t('User realated to route.'),
    ),
  );

  // timestamp field.
  $data[DB_TABLE_NAME]['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('Timestamp of route request.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );

  // duration field.
  $data[DB_TABLE_NAME]['duration'] = array(
    'title' => t('Duration'),
    'help' => t('Duration of route.'),
    'field' => array(
      'handler' => 'views_handler_field_time_interval',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_time_interval',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_time_interval',
    ),
  );

  // distance field.
  $data[DB_TABLE_NAME]['distance'] = array(
    'title' => t('Distance'),
    'help' => t('Distance of route.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE,
      'float' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  $data[DB_TABLE_NAME]['origin_coordinates'] = array(
    'title' => t('origin coordinates'),
    'help' => t('origin_coordinates (lat, lon)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data[DB_TABLE_NAME]['destination_coordinates'] = array(
    'title' => t('destination coordinates'),
    'help' => t('destination coordinates (lat, lon)'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data[DB_TABLE_NAME]['polyline'] = array(
    'title' => t('polyline'),
    'help' => t('compressed waypoints - Attention: could be a big amount of data!'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data[DB_TABLE_NAME]['provider'] = array(
    'title' => t('provider'),
    'help' => t('used route provider'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE, // This is use by the table display plugin.
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  return $data;

}

